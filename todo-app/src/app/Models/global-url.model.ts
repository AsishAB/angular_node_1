import { environment } from "src/environments/environment";


const API = environment.MasterAPI; //This can be directly used in all routes in Angular

export const api_url = API;
