export class Users {
  TUM_Id : number;
  TUM_First_Name : string;
  TUM_Last_Name : string;
  TUM_Gender: string;
  TUM_Email: string;
  TUM_Mobile: string;
  TUM_Country: string;
  TUM_State: string;
  TUM_City: string;
  TUM_Profile_Pic: string;
  TUM_Status:number=1;
  TUM_Deleted_Flag:boolean=false;
  TUM_Password?:string;
}
